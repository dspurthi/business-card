import React from "react";

import { Header } from "./components/header";
import { Footer } from "./components/footer";
import { MainContent } from "./components/content";

import "./App.css";

function App() {
  return (
    <div className="business-card">
      <Header />
      <MainContent />
      <Footer />
    </div>
  );
}

export { App };
