import React from "react";

import "./content.css";

function MainContent() {
  return (
    <div className="content-class">
      <div className="content-class-title">
        <div>Spurthi Dulipalla</div>
        <div>Python Programmer</div>
      </div>
      <div className="content-class-button">
        <a href="mailto:spurthi.dulipalla@gmail.com" target={"_blank"}rel="noreferrer">
          <button>Email</button>
        </a>
        <a href="https://www.linkedin.com/in/spurthi-dulipalla-90543067/" target={"_blank"}rel="noreferrer">
          <button>Linkdin</button>
        </a>
      </div>

      <div className="content-class-content">
        <h2>About</h2>
        <p>
          I'm a self-taught Python Programmer looking for an opportunity to
          enter into the software enigineering and IT Industry.
        </p>
        <h2>Skills</h2>
        <p>
          I have solved around 400 programming challenges in Leetcode and
          HackerRank. I also worked on few personal projects using Django. I did
          data-analysis using Pandas on freely available datasets. I can write
          SQL Queries too. Currently I'm learning React Js. As a part of that I
          developed this Card.
        </p>
      </div>
    </div>
  );
}
export { MainContent };
