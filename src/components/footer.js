import hr from "../HackerRank_logo.png";
import gitlab from "../gitlab.jpg";
import link from "../linkdin.png";
import lc from "../LeetCode_logo_black.png";

import "./footer.css";

function Footer() {
  return (
    <footer className="bottom-logos">
        <a href="https://leetcode.com/spurthid/" target={"_blank"} rel="noreferrer">
        <img src={lc} width={25} height={25} alt="leetcode" />
      </a>
      <a href="https://www.hackerrank.com/spurthi_d" target={"_blank"} rel="noreferrer">
        <img src={hr} width={25} height={25} alt="hrank" />
      </a>
      <a href="https://gitlab.com/users/dspurthi/starred" target={"_blank"}rel="noreferrer">
        <img src={gitlab} width={25} height={25} alt="glab" />
      </a>
      <a href="https://www.linkedin.com/in/spurthi-dulipalla-90543067/"target={"_blank"}rel="noreferrer">
        <img src={link} width={25} height={25} alt="linkdin" />
      </a>
    </footer>
  );
}
export { Footer };
