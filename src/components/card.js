import React from "react";
import spurthi from "../pic.jpg";

import "./card.css"

function Card() {
  return (
    <div className="card-special">
      <img src={spurthi} width={350} height={200} alt="pic" />
    </div>
  );
}

export { Card };
